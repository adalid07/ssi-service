/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Employee;
import org.springframework.stereotype.Service;

@Service
public interface EmployeeService extends GenericService<Employee> {
}
