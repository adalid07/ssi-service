/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services;

import org.springframework.data.repository.CrudRepository;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class GenericServiceImpl<T> implements GenericService<T> {

    @Override
    public List<T> findAll() {
        List<T> results = new ArrayList<>();
        getRepository().findAll().iterator().forEachRemaining(results::add);
        return results;
    }

    @Override
    public T findById(Long id) {
        Optional<T> optional = getRepository().findById(id);
        if (!optional.isPresent()) {
            throw new RuntimeException(
                    getType() + " NotFound");
        }
        return optional.get();
    }

    private String getType() {
        String typeName = (((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0]).getTypeName();
        typeName = typeName.substring(typeName.lastIndexOf(".") + 1);
        return typeName;
    }

    protected abstract CrudRepository<T, Long> getRepository();
}
