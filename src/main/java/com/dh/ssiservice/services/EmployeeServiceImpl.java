/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Employee;
import com.dh.ssiservice.repositories.EmployeeRepository;
import org.springframework.data.repository.CrudRepository;

public class EmployeeServiceImpl extends GenericServiceImpl<Employee> {
    private EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    protected CrudRepository<Employee, Long> getRepository() {
        return employeeRepository;
    }
}
