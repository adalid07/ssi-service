package com.dh.ssiservice.model;

import javax.persistence.Entity;

@Entity
public class BuyOrder {
    private Long price;
    private String place;

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
}
