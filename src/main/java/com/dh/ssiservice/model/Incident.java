package com.dh.ssiservice.model;

import javax.persistence.Entity;
import java.util.Date;

@Entity
public class Incident {
    private Long id;
    private Date date;
    private String description;
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



}
